#!/bin/bash

# fun: make_aequence

# sort.py to func in ISchecker for ip_sort and --ips
# test:
 # for i in '101,102,103,104,105,200' '1,2,3,4,5,6,7' '1,2,4,5,6,9,12' '3,4,5,6,8,9,10' '1,3,5,8,9,11,12' '1,3,4,5,6,7,8,9,11,12' ; do echo $i ;bash sort_fun.sh "$i" ;echo ;done 

function make_sequence() {

# takes a list then gets only numbers finally returns a numbers sequences which can genarate the original list of numbers.   example: '1,2,3,4,5,7,9' -> '{1..5},7,9'
# $1 should be a list with numbers!  (-d [ ,] )
# $2 = flag for language. for example bash ; python . If you want your customs 
# $3 = sequence separator by default for bash '..'
# $4 = sequence brackets (begin)  by default for bash '{' 
# $5 = sequence brackets (ending) by default for bash '}' 
# $6 = separator for numbers by default for bash ',' . space by default for set mode

if [ "$2" == 'bash' -o "$2"  == '' ] ; then \
 # {1..9},2,3,4,{4..7}
 local seq_brackers_b='{'
 local seq_separator='..'
 local seq_brackers_e='}'
 local separator=','
elif [ "$2" == 'set' ] ; then \
 # {seq_brackers_b}[0-9]{seq_separator}[0-9]{seq_brackers_e}{separator}
  local seq_brackers_b="$3"
  local seq_separator="$4" ; [ -z "$seq_separator" ] && seq_separator='none'
  local seq_brackers_e="$5"
  local separator="$6"
fi;
if [ -z "$separator" ] ; then seq_brackers_b='{';seq_separator='..';seq_brackers_e='}';separator=',' ;fi  # set by default if there is not enough keys
local ar=($(echo $1|sed 's/[^0-9 ,]//g' |sed 's/[ ,]\+/ /g'|tr " " "\n"|sort -gu|tr "\n" " "))   # crreate massive with list of numbers (only number and uniq)
# echo $1|sed 's/[^0-9 ,]//g' |sed 's/[ ,]\+/ /g'|tr " " "\n"|sort -gu|tr "\n" " "
local min='No' # flag as min-variable 
local total=() # return massive - output-variable
 if (( ${#ar[*]} <= 3 )) ; then  echo -n ${ar[*]}|sed "s/ /${separator}/g"; 

 else \
 for ((i=0; i<${#ar[*]}; i++));

   do if [ $min != 'No' ] ;
        then \
           if (($min + 1 == ${ar[$i]} && $i != ${#ar[*]} - 1 && ${ar[$i]} + 1 == ${ar[$i+1]} )) ;
            then min=${ar[i]} ;
           elif (( $i != ${#ar[*]} - 1 && ${ar[$i]} + 1 == ${ar[$i+1]} )) ;
            then total+=(${ar[i]}) ;
                 (( $i < ${#ar[*]} - 2 )) && (( ${ar[$i]} + 1 == ${ar[$i+1]} && ( ${ar[$i]} + 2 != ${ar[$i+2]} || $i == ${#ar[*]} - 1 ) )) && \
                  total+=($separator) ;
                 min=${ar[$i]}; 

             else  (( ${ar[$i - 1]} +1 == ${ar[$i]} && ${total[ ${#total[*]} - 1 ]}  < ${ar[$i]} - 1 )) && \
                      total+=($seq_separator) ;
                   total+=(${ar[i]}) ;

                   if (( $i < ${#ar[*]} - 2 ));
                    then (( ${ar[$i]} + 1 < ${ar[$i+1]} || ${ar[$i]} + 2 < ${ar[$i+2]} )) &&  total+=($separator) ;
                    elif [ "${total[${#total[*]}-2]}" == "$seq_separator" ] ; then (( $i == ${#ar[*]} - 2 )) && total+=($separator) ; fi;

                   min='No' ; # echo 'else-set-No'
             fi;      
      else min=${ar[$i]};
           total+=(${ar[i]}) ;

           if (( $i != ${#ar[*]} - 1 &&  ${ar[$i]} + 1 < ${ar[$i+1]} )) ; then total+=($separator) ;
           elif (( $i != ${#ar[*]} - 2 &&  ${ar[$i]} + 2 < ${ar[$i+2]} ))  ;  then total+=($separator) ;
           elif (( $i == ${#ar[*]} - 2 )) ; then  total+=($separator) ; fi;
        fi;
 done 2>/dev/null
seq_separator="$(echo $seq_separator|sed 's/\./\\./g')"

if [ -z "$separator" ] ; 
  then echo -n ${total[*]}|sed "s/ \(${seq_separator}\) /\1/g" ;   # if sepator is a space will fix space between seq_brackets
else echo -n ${total[*]}|tr -d ' ' ; fi  |sed "s/\([0-9]\+${seq_separator}[0-9]\+\)/${seq_brackers_b}\1${seq_brackers_e}/g"
fi;
}

# ips sort fun
 function ip_sort_fun() {
# input:  $1 = list of ips(without empties!) ; $2 = part of ip which should be sorted f.e. 4 = /24
# output: sorted ips in one line   f. e. $1 = 10.200.1.1; 10.200.1.5 $2=4 --> 10.200.1.[1,5]
local ip_end ip24 ip24_sort;   # ip24_sor contains sequence of variable ips
local ip_cut1=$(echo `bash -c "echo {$2..4}"`|sed ' s/ /,/g');       # var for cut - exmpl for $2=3: '3,4' ; $2=4: '4'
local ip_cut2=$(echo `bash -c "echo {$(echo $(($2+1)))..5}"`|sed ' s/ /,/g' ); # exmpl for $2=3: '4' ; $2=2: '3,4'
local ip_begin=$(echo "$1"| sed 's/ /\n/g'|sort| sed 's/\./ /g'| cut -d ' ' -f $ip_cut1 --complement| sed 's/\( \|$\)/./g'|sort|uniq);
for ip_b in $ip_begin ;
   do ip_end=$(echo "$1"|grep -- "$ip_b"|sed 's/\./ /g'|cut -d ' ' -f $ip_cut2|sort|uniq|sed 's/^/./g; s/ /./g');
     for ip_e in $ip_end;
       do ip24_sort=$(echo "$1"|sed 's/ /\n/g'|grep -- "^$ip_b"|grep -- "$ip_e$"|sort|uniq|sed 's/\./ /g'|cut -d ' ' -f $2|sort|uniq) ;
       ip24=$(echo $ip_b'{'$(make_sequence "$(echo $ip24_sort|sed 's/ /,/g')" 'bash' )'}'$ip_e| sed 's/\(^\.\|\.$\)//g');
       if echo $ip24 | grep -E '\{[0-9]{1,3}\}' 1>/dev/null ; then echo -n $ip24' '| tr -d '{}' ;
       else echo -n $ip24' '; fi  |sed 's/{\({[0-9]\+\.\.[0-9]\+}\)}/\1/g'  # removed {{[0-9]..[0-9]}}
     done;
  done;
  }
 make_sequence $@

# ip_sort_fun "$1" '4'