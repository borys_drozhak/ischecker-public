IS health-check script for Onapp-store IS package.  (#!/bin/bash)

Shows IS state and finds out common mistakes of cloud configuration.

From the author:
 Please send the ticket number to borys.drozhak@gmail.com if the script fails or has errors. I'll try to correct the issues. Also please let me know if you want add something in script
 Also You can create issue/bug on bitbucket.


```
# Main modes:
# [ -N | --network ]
#        - check MTU conformity for each hvs zone
#        - shows packets errors/dropped/overruns for each interface in onappstore bridge
#        - Link detected for all NICs and interfaces in SAN network
#        - check bond_mode conformity
#        - ping with Jumbo frame(include backups servers) -deadline 1,5 s
#        - report error if onappstoresan bridge is absent
#        - report ssh issues
#        - check multicast enabled snooping on san bridge
# [ -D | --deeply ] - deep mode: ping with 1000 packets with deadline 5 sec ; Works only for Network key
#
# [ -S | --isstate ]
#        - check telnet/ssh connection
#        - status and statistic of nodes
#        - checking Overflowed nodes (100%) inside storage vm
#        - I/O errors on nodes
#        - xfs corrupting and issues
#        - check groupmon/api/redis processes (12/7/4)
#        - check failed API-call transaction:`nodelocalinfo` inside stvms
#        - check isd processes on HV and stvm
#        - check dropbear(ssh) on stvm
#        - check running isd and groupmon at one time
#        - check crond and crontab default values
#        - report overlays on HVs
#        - check diskhotplug metadata lose
#        - report large timeout of onappstore nodes command on HVs
#        - check lock files - pinglock, freeinuselock, diskinfo.txn and sort files [.sort]
#        - check conformity groupmon processes keys in particularly hvs zone
#        - report group/isd issues
#        - check onapp-store versions conformity (controller and package)
#        - check hanged telnet transactions (telnet session which running more then 100 sec and hanged onappstore nodes transactions)
# [ -T ] - check via telnet method (blocked ssh-method)
# [any of keys]   -  check mess of onapp-store packets.

# [-V| --version] -  show current version of ISchecker.
# [--list]        -  shows list of cloudboot HVs
# [--rpmlist]     -  source rpm packets of OnApp Storage Versions
# [--tips]        -  shows tips for troubleshooting of some of IS issues.
# [--update]      -  Update ISchecker to newest version from ischeck repo by re-writing itselt in current directory

# vm/vdisk modes:
#
# -d : {vdisks mode} - checking vdisks ; -o specify ip of HV 
#     requiers an argument: [ 'all' | 'degraded' | 'vdisk_indetifier' ]
#     all               - check all vdisks 
#     degraded          - check all degraded vdisks
#     vdisk_identifier  - check specified vdisk
# --vm= : {vm mode}  - checking vdisks of specified vm by  [vm_identifier] 

# Exit codes:
#   1 :  no info from db
#   2 :  invalid options
#   3 :  no onapp-store packets
```